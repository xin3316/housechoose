/**
 * Created by jf on 2015/9/11.
 * Modified by bear on 2016/9/7.
 */
$.ajaxSetup({  
    async : false  
});
var pubManager = {
    $container: $('#container'),
    _list:[],//数据列表
    _perpage_max_item:40,//默认设置每页最大显示数量
    _group_no:0,//当前组号
    _last_group_no:0,//最后显示组号
    indexno:"",
    showallbox:false,
    groupsize:40,//每组数据个数
    pubcount:0,//已公示数据数量
    screen_group_count:1,//每屏显示多少组
    _max_group_no:0,//最大组号
    refresh_sec:6000,
    timer:null,
    _timer_refresh:null,
    fmtfun:function(info){return info.code;},
    init_socket:function(){//WebSocket数据通讯，通讯OK，暂时不用
    	var socket = new WebSocket('ws://127.0.0.1:8118');
    	socket.onopen = function(event) {
    	    alert('连接');
    	};

    	socket.onmessage = function(event) {
    	    var content = event.data;
    	    if (content.length > 2) {
    	        alert(content);
    	    }
    	    
    	    if(content==""){
    	    	
    	    }
    	};
    	var send = document.getElementById('send');
    	send.addEventListener('click', function() {

    	    var content = '123456789'
    	    socket.send(content);
    	    alert('发送');

    	});
    },
    //主动方式，获取最新数据
    _refresh_list:function(tmp_group_no){
    	console.log('readGroupData_Start:'+tmp_group_no);    
    	var da=null;
    	
    	//读取当前选择的组数据
    	$.get("/index.php/home/index/group_result/groupno/"+tmp_group_no, function(data){
    		//展示组数据
    		if(!data||data.length==0){
    			tmp_group_no=0;
    			console.log('readGroupData_End:'+tmp_group_no);
    			da=null;
    		}
    		else{
    			console.log('readGroupData_End:'+tmp_group_no);   
    			da=data;      
    		}
    	});
    	return da;
    },
    play:function(){
    	if(!this.timer){
    		this.loop();
    	}else{
    		clearInterval(this.timer);
    		this.timer=null;
    	}
    	this.timer=setInterval(function(){ resultManager.loop();},resultManager.refresh_sec);
    },
    pause:function(){
    	if(!this.timer){
    		this.play();
    	}else{
    		clearInterval(this.timer);
    		this.timer=null;
    	}
    },
    loop:function(){
    	//第一步：初始化参数
    	//1、存储最后一次获取前的组号
    	this._last_group_no=this._group_no;
    	//2、读取到最新组号
    	var ser_max_group_no=this._read_group_no();
    	if(!ser_max_group_no)
        	this._max_group_no=0;
    	else
    		this._max_group_no=ser_max_group_no;
    	
    	//3、如果最新组号
    	if(this._max_group_no>this._last_group_no){
    		//有新数据了，读取并存储一下
        	for(var i=this._last_group_no;i<=this._max_group_no;i++){
            	//如果未读取过该数据，则读取一次
            	if(!this._list[i]){
            		this._list[i]=this._read_group_data(i);
            	}
        	}
        	//显示最新的未显示的第一组
        	this._group_no=this._group_no+1;
    	}else if(this._max_group_no<=this._last_group_no){    		
    		this._group_no=this._max_group_no>0?1:0;
    	}

    	//4、清空UI显示
		$('section.page_left ul li').removeClass("active");		
		$('section.page_main').empty();
		
		//5、显示最大组号
    	$("#maxgroupno").html(this._max_group_no);
    	
    	//6、最新组号没有最后显示的组号大，说明活动重新开始了，这时候刷新系统    	
		if(this._group_no==0&&this._max_group_no==0){
			$('#pages').hide();
			$('#ready').show();
			this._list=[];
		}
		else if(this._group_no>0){
			$('#pages').show();
			$('#ready').hide();
			//7、显示左侧列表
			this._create_group_ul(this._max_group_no);//创建左侧列表
			//8、创建并显示指定组数据UI
        	this._create_result_ul(this._list[this._group_no],this._group_no);//创建右侧结果数据列表
        	
        	//9、设置激活状态
        	$('section.page_left ul li[data-id='+this._group_no+']').addClass("active");
        	
        	while(this._group_no%this.screen_group_count!=0&&(this.showallbox||this._list[this._group_no+1]&&this._list[this._group_no+1].length>0)){
        		this._group_no++;
        		//8、创建并显示更多指定组数据UI
        		this._create_result_ul(this._list[this._group_no],this._group_no);//创建右侧结果数据列表

            	//9、设置激活状态
            	$('section.page_left ul li[data-id='+this._group_no+']').addClass("active");
			}
		}
    	var self=this;
    	$("section.page_left ul li").click( function () {
    		self._group_no=parseInt($(this).html())-1;
    		self.loop();
    	});
    },
    
    _create_result_ul:function (data,var_group_no){
    	var screen_item_count=this.groupsize*this.screen_group_count;
    	var ul=$('<ul></ul>');
    	
    	var self=this;
    	if(data&&data.length>0){
	    	data.forEach(function(item,index){
	    		//console.log(item);
	    		var info= JSON.parse(item.col2);
	    		var li=$('<li></li>');
	    		li.append('<label class="no'+screen_item_count+'">'+item.col1+'</label><label class="code'+screen_item_count+'">'+((typeof(self.fmtfun)!='function')?info.code:self.fmtfun(info))+'</label>');
	    		li.appendTo(ul);
	    		if((index+1)%10===0||item.col1==self.pubcount){
	    			if(self.showallbox&&item.col1==self.pubcount&&item.col1%10>0){
						for(var j=0;j<10-(item.col1%10);j++){
							var li_null=$('<li></li>');
							li_null.append('<label class="no null">'+(self.pubcount+j+1)+'</label><label class="code null">0000</label>');
							li_null.appendTo(ul);
						}
					}
	    			if(index>0||item.col1==self.pubcount){
	    				$('section.page_main').append(ul);
	    			}
	    			ul=$('<ul></ul>');
	    		}
	    	});
    	}else{
    		for(var i=0;i<self.groupsize;i++)
    		{
    			var index=(var_group_no-1)*self.groupsize+i;
    			if(index+1>self.pubcount) break;
    			var li=$('<li></li>');
    			li.append('<label class="no'+screen_item_count+'">'+(index+1)+'</label><label class="code'+screen_item_count+' null">0000</label>');
    			li.appendTo(ul);
    			if((index+1)%10===0||index+1==self.pubcount){
    				if((index+1)==self.pubcount&&(index+1)%10>0){
    					for(var j=0;j<10-((index+1)%10);j++){
    						var li_null=$('<li></li>');
    						li_null.append('<label class="no'+screen_item_count+' null">'+(self.pubcount+j+1)+'</label><label class="code'+screen_item_count+' null">0000</label>');
    						li_null.appendTo(ul);
    					}
    				}
    				if(index>0){
    					$('section.page_main').append(ul);
    				}
    				ul=$('<ul></ul>');
    			}
    		}
    	}
    },
    _create_group_ul:function(maxgroupno){
    	$('section.page_left').empty();
    	var ul=$('<ul></ul>');
    	var start_no=Math.floor(this._group_no/30);
    	for(var i=start_no*30;i<maxgroupno;i++){
    		var li=$('<li></li>');
    		li.attr("data-id",i+1);
    		li.append(i+1);
    		li.appendTo(ul);
    		//一次性加载所有数据
    		/*if(!this._list[i+1]||this._list[i+1].length==0){
    			this._read_group_data(i+1);
    		}*/
    	}
    	$('section.page_left').append(ul);
    }
}
