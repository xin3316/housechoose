<?php
/**
 * 配置文件
 */
return array(
    'DB_TYPE' => 'mysql',
    'DB_HOST'   => '127.0.0.1', // 服务器地址
    'DB_NAME'   => 'xinhouse', // 数据库名
    'DB_USER'   => 'root', // 用户名
    'DB_PWD'    => '',  // 密码
    'DB_PORT'   => '3306', // 端口
    'DB_PREFIX' => 'xh_',
    //密钥
    "AUTHCODE" => 'VeLyOux9L17OySMSuc',
    //cookies
    "COOKIE_PREFIX" => '1MCMdD_',
);
