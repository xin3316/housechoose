<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;">
<title>列表</title>
<!-- <link rel="stylesheet" href="/public/css/jquery.mobile-1.4.5.min.css">-->
<link href="/public/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
<link rel="stylesheet" type="text/css" href="/public/css/style.css">

<script src="/public/js/jquery-1.8.3.min.js"></script>
<script src="/public/js/index.js"></script>
<!--<script src="/public/js/jquery.mobile-1.4.5.min.js"></script>-->

</head>
<body> 
<div class="page">
     <div class="all-width red-bg white-color normal-font-size p-h-sm p-w-sm remind_header">
     	<p class="right"><i class="m-r-xs fa fa-book"></i>帮助</p>
      	<i class="m-r-xs fa fa-clock-o"></i>正在进行&nbsp;&nbsp;03分31秒4
     </div>
     <div class="white-bg m-b-xs check_list_title">
     	<dl class="tab_title">
        	<dd class="text-center left p-h-sm on">今日开盘</dd>
            <dd class="text-center left p-h-sm">我收藏的</dd>
        </dl>
     </div>
     
     <div class="tab_con">
     	<div class="tab_con_obj"> 
     
             <div class="white-bg m-b-xs gray-top normal-font-size today_house">
                <div class="gray-bottom p-w-sm p-h-sm bold-weight today_unit">
                    <p class="right">
                        <img class="left m-r-md" src="/public/images/tishi.png">
                        <img class="left" src="/public/images/xiala.png"  onclick="ifShow(this,'.door_list','.today_house')">
                    </p>
                    <p>20号楼</p>
                </div>
                <div class="door_list">
                    <div class="door_con">
                        <div class="gray-bottom p-w-sm p-h-sm today_unit">
                            <p class="right"> 
                                <img class="left" src="/public/images/xiala.png" onclick="ifShow(this,'.model_list','.door_con')">
                            </p>
                            <p>西户 - 四房两厅两卫（c1户型）</p>
                        </div>
                        <dl class="model_list">
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">20#2601</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom collect">
                                <p class="right text-center"><span><i class=" fa fa-star"></i></span><br>121</p>
                                <p class="left p-l-xs">20#2501</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">20#2401</p>
                                <p class="left">98.555万</p>
                            </dd>   
                        </dl> 
                    </div> 
                    <div class="door_con">
                        <div class="gray-bottom p-w-sm p-h-sm today_unit">
                            <p class="right"> 
                                <img class="left" src="/public/images/xiala.png" onclick="ifShow(this,'.model_list','.door_con')">
                            </p>
                            <p>南1户 - 两房两厅一卫（f1户型）</p>
                        </div>
                        <dl class="model_list">
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom collect">
                                <p class="right text-center"><span><i class=" fa fa-star"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>   
                        </dl> 
                    </div> 
                    <div class="door_con">
                        <div class="gray-bottom p-w-sm p-h-sm today_unit">
                            <p class="right"> 
                                <img class="left" src="/public/images/xiala.png" onclick="ifShow(this,'.model_list','.door_con')">
                            </p>
                            <p>南2户 - 两房两厅一卫（f2户型）</p>
                        </div>
                        <dl class="model_list">
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom collect">
                                <p class="right text-center"><span><i class=" fa fa-star"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>   
                        </dl> 
                    </div> 
                </div>
             </div>
             
             <div class="white-bg m-b-xs gray-top normal-font-size today_house">
                <div class="gray-bottom p-w-sm p-h-sm bold-weight today_unit">
                    <p class="right">
                        <img class="left m-r-md" src="/public/images/tishi.png">
                        <img class="left" src="/public/images/xiala.png"  onclick="ifShow(this,'.door_list','.today_house')">
                    </p>
                    <p>23号楼/1单元</p>
                </div>
                <div class="door_list">
                    <div class="door_con">
                        <div class="gray-bottom p-w-sm p-h-sm today_unit">
                            <p class="right"> 
                                <img class="left" src="/public/images/xiala.png" onclick="ifShow(this,'.model_list','.door_con')">
                            </p>
                            <p>01（c1户型）</p>
                        </div>
                        <dl class="model_list">
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom collect">
                                <p class="right text-center"><span><i class=" fa fa-star"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>   
                        </dl> 
                    </div> 
                    <div class="door_con">
                        <div class="gray-bottom p-w-sm p-h-sm today_unit">
                            <p class="right"> 
                                <img class="left" src="/public/images/xiala.png" onclick="ifShow(this,'.model_list','.door_con')">
                            </p>
                            <p>02（f1户型）</p>
                        </div>
                        <dl class="model_list">
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom collect">
                                <p class="right text-center"><span><i class=" fa fa-star"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>   
                        </dl> 
                    </div> 
                </div>
             </div>
              
      	</div>
        <div class="tab_con_obj hide">
         
             <div class="white-bg m-b-xs gray-top normal-font-size today_house">
                <div class="gray-bottom p-w-sm p-h-sm bold-weight today_unit">
                    <p class="right">
                        <img class="left m-r-md" src="/public/images/tishi.png">
                        <img class="left" src="/public/images/xiala.png"  onclick="ifShow(this,'.door_list','.today_house')">
                    </p>
                    <p>我收藏的</p>
                </div>
                <div class="door_list">
                    <div class="door_con">
                        <div class="gray-bottom p-w-sm p-h-sm today_unit">
                            <p class="right"> 
                                <img class="left" src="/public/images/xiala.png" onclick="ifShow(this,'.model_list','.door_con')">
                            </p>
                            <p>01（c1户型）</p>
                        </div>
                        <dl class="model_list">
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom collect">
                                <p class="right text-center"><span><i class=" fa fa-star"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>   
                        </dl> 
                    </div> 
                    <div class="door_con">
                        <div class="gray-bottom p-w-sm p-h-sm today_unit">
                            <p class="right"> 
                                <img class="left" src="/public/images/xiala.png" onclick="ifShow(this,'.model_list','.door_con')">
                            </p>
                            <p>02（f1户型）</p>
                        </div>
                        <dl class="model_list">
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom collect">
                                <p class="right text-center"><span><i class=" fa fa-star"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>   
                        </dl> 
                    </div> 
                </div>
             </div>
             
             <div class="white-bg m-b-xs gray-top normal-font-size today_house">
                <div class="gray-bottom p-w-sm p-h-sm bold-weight today_unit">
                    <p class="right">
                        <img class="left m-r-md" src="/public/images/tishi.png">
                        <img class="left" src="/public/images/xiala.png"  onclick="ifShow(this,'.door_list','.today_house')">
                    </p>
                    <p>010号楼/1单元</p>
                </div>
                <div class="door_list">
                    <div class="door_con">
                        <div class="gray-bottom p-w-sm p-h-sm today_unit">
                            <p class="right"> 
                                <img class="left" src="/public/images/xiala.png" onclick="ifShow(this,'.model_list','.door_con')">
                            </p>
                            <p>01（c1户型）</p>
                        </div>
                        <dl class="model_list">
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom collect">
                                <p class="right text-center"><span><i class=" fa fa-star"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>   
                        </dl> 
                    </div> 
                    <div class="door_con">
                        <div class="gray-bottom p-w-sm p-h-sm today_unit">
                            <p class="right"> 
                                <img class="left" src="/public/images/xiala.png" onclick="ifShow(this,'.model_list','.door_con')">
                            </p>
                            <p>02（f1户型）</p>
                        </div>
                        <dl class="model_list">
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom collect">
                                <p class="right text-center"><span><i class=" fa fa-star"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>
                            <dd class="p-w-sm p-h-xs gray-bottom">
                                <p class="right text-center"><span><i class=" fa fa-star-o"></i></span><br>121</p>
                                <p class="left p-l-xs">B区2#1202</p>
                                <p class="left">98.555万</p>
                            </dd>   
                        </dl> 
                    </div> 
                </div>
             </div>
              
        </div>
     </div>
   
    <div style="line-height:0; height:5rem;"></div>
    <div class="footer">
    	<div class="footer_box white-bg gray-top">
        	<dl class="normal-font-size text-center">
            	<dd class="left gray-right">
                	<a href=""><span><i class="fa fa-commenting"></i></span><br>
                    咨询</a>
                </dd>
                <dd class="left gray-right">
                	<a href=""><span><i class="fa fa-bell"></i></span><br>
                    提醒</a>
                </dd> 
                <dd class="left">
                	<a href=""><span><i class="fa fa-star"></i></span><br>
                    收藏</a>
                </dd>
                <dd class="left">
                	<a href="">查看保证金</a>
                </dd>
                <dd class="left">
                	<a href="">看相似拍品</a>
                </dd>
            </dl>
        </div> 
    </div>   
</div>  
  
<div class="hide_black"></div>
    

<script type="text/javascript">

	$(function(){
		//选项卡切换 
		$(".tab_title").children("dd").click(function(){
			$(this).addClass("on").siblings().removeClass("on");
			var num=$(".tab_title>dd").index(this);
			var tabobj=$(".tab_con").children(".tab_con_obj");
			tabobj.hide(); 
			tabobj.eq(num).show();
		}); 
	})
	
 	//点击隐藏显示切换
	function ifShow(which,obj,father){ 
		var obj=$(which).parents(father).children(obj); 
		if(obj.css('display')=='block'){ 
				obj.css({
					'display':'none' 
				})
				$(which).attr('src','/public/images/xiala.png');
			}
			else if(obj.css('display')=='none'){
				obj.css({
					'display':'block'
				})
				$(which).attr('src','/public/images/shangla.png');
			} 
	}
</script> 

</body>
</html>