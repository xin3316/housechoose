<?php
namespace Home\Controller;
use Common\Controller\HomebaseController;

class IndexController extends HomebaseController{
	
	function index(){
		//echo "hello";
		$this->display(":index");
	}
	function info(){
	    //echo "hello";
	    $this->display(":info");
	}
	function login(){
	    //echo "hello";
	    $this->display(":login");
	}
	function lst(){
	    //echo "hello";
	    $this->display(":lst");
	}
	function buy(){
	    //echo "hello";
	    $this->display(":buy");
	}
}